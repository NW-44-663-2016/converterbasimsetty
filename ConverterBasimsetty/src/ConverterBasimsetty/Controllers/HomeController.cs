﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using ConverterBasimsetty.Models;

namespace ConverterBasimsetty.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewData["Title"] = "ConverterApp by Basimsetty";
            ViewData["Result"] = "";
            ViewData["Value"] = "";
            Converter converter = new Converter();
            return View(converter);
        }

        public IActionResult Convert(Converter converter)
        {
            if (ModelState.IsValid)
            {

                int abc = (int)((converter.Temperature_F - 32) * 5.0 / 9.0);
                ViewData["Title"] = "ConverterApp by Basimsetty";
                ViewData["Result"] = "Temperature in C = " + abc;
                ViewData["Value"] = abc;
            }
            return View("Index", converter);
        }
    }
}
